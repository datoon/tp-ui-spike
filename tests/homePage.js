module.exports = {
  after: browser => { 
    browser.end()
  },

  'loads correctly': (browser) => {
    browser.page.home()
      .navigate()
      .waitForElementVisible('@winplace', 30*1000)

    const viewports = [
      { name: 'desktop', height: 2024, width: 1025 },
      { name: 'iPhoneX', height: 812,  width: 375 },
      { name: 'iPad', height: 1024,  width: 768  }
    ]

    viewports.forEach(viewport => {
      browser
      .resizeWindow(viewport.width, viewport.height)
      .compareScreenshot(viewport)
    })
  }
}