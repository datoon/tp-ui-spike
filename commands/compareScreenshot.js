exports.command = function(viewport, callback) {
  var self = this

  self.screenshot(false, image => {
    self.assert.compareScreenshot(viewport, image, result => {
      if(typeof callback === 'function') {
        callback.call(self, result)
      }
    })
  })

  return this;
}