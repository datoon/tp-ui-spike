const timeout = 30*1000

const homeCommands = {
  login: function(username, password) {
    return this.waitForElementVisible('@username', timeout)
      .setValue('@username', username)
      .setValue('@password', password)
      .click('@login')
  },

  addWinPlaceBet: function(stakeAmount) {
    return this.waitForElementVisible('body', timeout)
      .waitForElementVisible('@winplace', timeout)
      .click('@winplace')
      .waitForElementVisible('@stakeInput', timeout)
      .setValue('@stakeInput', stakeAmount)
      .waitForElementVisible('@placeBet', timeout)
      .click('@placeBet')
  }
}

module.exports = {
  url: 'http://ta2.totepool.com',
  commands: [homeCommands],
  elements: {
    winplace: {
      selector: '//*[@id="container"]/div[1]/div[1]/div/div/div[2]/div/div/div[6]/div[2]/div[1]/div[4]/button',
      locateStrategy: 'xpath'
    },
    stakeInput: {
      selector: '//*[@id="betslip"]/span/div[1]/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/input',
      locateStrategy: 'xpath'
    },
    placeBet: {
      selector: '//*[@id="betslip"]/div[2]/div[3]/div/div/span[1]/button',
      locateStrategy: 'xpath'
    },
    username: {
      selector: '/html/body/div[3]/div/div/div/form/div/div[1]/div/input',
      locateStrategy: 'xpath'
    },
    password: {
      selector: '/html/body/div[3]/div/div/div/form/div/div[2]/div/input',
      locateStrategy: 'xpath'
    },
    login: {
      selector: '/html/body/div[3]/div/div/div/form/div/button',
      locateStrategy: 'xpath'
    }
  }
}