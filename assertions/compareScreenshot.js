const Eyes = require('eyes.images').Eyes
const eyes = new Eyes()
eyes.setApiKey('9tzBo102Q104rfq7CA0V98lVfV3Y3IM59pg0rpNMgOOcBYZc110')

exports.assertion = function(viewport, image) {
  this.message = 'Unexpected screenshot error'
  this.expected = true

  this.command = callback => {
    eyes.open('Totepool.com', `Home Page - ${viewport.name}`, { height: viewport.height, width: viewport.width })
      .then(() => eyes.checkImage(image.value, viewport.name))
      .then(() => eyes.close(false), () => eyes.abortIfNotClosed())
      .then(results => callback(results))

    return this
  }

  this.value = result => true

  this.pass = isSame => {
    if(isSame) 
      this.message = 'Passed screenshot comparison'
    else
      this.message = 'Failed screenshot comparison'

    return isSame
  }
}